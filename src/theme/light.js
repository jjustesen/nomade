export const light = {
  title: 'light',

  colors: {
    primary: '#80CBC4',
    darkPrimary: '#4F9A94',
    lightPrimary: '#B2FEF7',
    black: '#323232',
    lightGray: '#EAEAEA'
  },

  fontSizes: {
    sx: '14px',
    sm: '18px',
    md: '24px'
  }
}

export default light
