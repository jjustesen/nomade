import { ThemeProvider } from 'styled-components'
import GlobalStyle from './GlobalStyle'
import light from '../theme/light'

export const Provider = ({ children }) => {
  return (
    <>
      <ThemeProvider theme={light}>
        <GlobalStyle />
        {children}
      </ThemeProvider>
    </>
  )
}

export default Provider
