import NmdHeaderPage from './components/layout/HeaderPage/HeaderPage'
import NmdCard from './components/molecules/Card/Card'
import PageHome from './pages/Page/Home/PageHome'
const App = () => {
  return (
    <>
      <NmdHeaderPage />
      <PageHome />
    </>
  )
}

export default App
