import React from 'react'
import styled from 'styled-components'
import NmdButton from '../../../components/atoms/Button/Button'
import { NmdBox } from '../../../components/elements/Box'
import { NmdFlex } from '../../../components/elements/Flex'
import { NmdCol, NmdRow } from '../../../components/elements/Grid'
import { NmdText } from '../../../components/elements/Text'
import NmdCard from '../../../components/molecules/Card/Card'
import { NmdSearchBar } from '../../../components/molecules/SearchBar'

export const PageHome = ({ nome = 'Nome do lugar', cidade = 'cidade', valor = '100,00', ...props }) => {
  const NmdBackgroundImage = styled(NmdFlex)`
    width: 100vw;
    height: 70vh;
    box-shadow: inset 561px 70vh 0px rgba(0, 0, 0, 0.4);

    flex-direction: column;
    justify-content: space-between;
    align-items: center;
    background-position: center;
    background-size: cover;
    background-image: url('https://storage.googleapis.com/instaviagem-wordpress/2018/11/10-Melhores-lugares-para-lua-de-mel-no-Caribe-Punta-Cana-Shutterstock-4-1500x500.jpg');
  `

  return (
    <>
      <NmdBackgroundImage {...props} backgroundColor="gray" py={64}>
        <NmdSearchBar />

        <NmdButton color="darkPrimary" label="Crie seu proprio roteiro" />
      </NmdBackgroundImage>
      <NmdRow mx={80}>
        <NmdCol width={1 / 3}>
          <NmdCard />
        </NmdCol>
        <NmdCol width={1 / 3}>
          <NmdCard />
        </NmdCol>
        <NmdCol width={1 / 3}>
          <NmdCard />
        </NmdCol>
      </NmdRow>
    </>
  )
}

export default PageHome
