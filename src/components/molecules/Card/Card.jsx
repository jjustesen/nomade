import React from 'react'
import styled from 'styled-components'
import { NmdBox } from '../../elements/Box'
import { NmdFlex } from '../../elements/Flex'
import { NmdText } from '../../elements/Text'

export const NmdCard = ({ nome = 'Nome do lugar', cidade = 'cidade', valor = '100,00', ...props }) => {
  const NmdCard = styled(NmdFlex)`
    border-radius: 26px;
    min-height: 190px;
    flex-direction: column;
    justify-content: space-between;
    overflow: hidden;
  `

  return (
    <NmdCard {...props} backgroundColor="lightGray">
      <NmdFlex
        backgroundColor="darkPrimary"
        borderRadius="0px 0px 18px 0px"
        width="fit-content"
        mr="4px"
        px="12px"
        py="8px"
      >
        <NmdText fontSize="sm" fontWeight="600" color="white">{`R$ ${valor}`}</NmdText>
      </NmdFlex>

      <NmdFlex justifyContent="space-between" alignItems="center" pb={22} px={14} width={1}>
        <NmdFlex flexDirection="column">
          <NmdText fontSize="sm" fontFamily="montserrat" fontWeight="600">
            {nome}
          </NmdText>
          <NmdFlex alignItems="center">
            <NmdBox backgroundColor="black" width={24} height={24} mr="4px" />
            <NmdText fontSize="sx" fontFamily="montserrat" fontWeight="600" opacity="50%">
              {cidade}
            </NmdText>
          </NmdFlex>
        </NmdFlex>

        <NmdFlex>
          <NmdBox backgroundColor="primary" width={24} height={24} mr="4px" />
          <NmdBox backgroundColor="primary" width={24} height={24} mr="4px" />
          <NmdBox backgroundColor="primary" width={24} height={24} mr="4px" />
          <NmdBox backgroundColor="primary" width={24} height={24} mr="4px" />
          <NmdBox backgroundColor="primary" width={24} height={24} mr="4px" />
        </NmdFlex>
      </NmdFlex>
    </NmdCard>
  )
}

export default NmdCard
