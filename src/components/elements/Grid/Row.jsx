import styled from 'styled-components'

import { NmdFlex } from '../Flex'

export const NmdRow = styled(NmdFlex)``

NmdRow.displayName = 'NmdRow'

NmdRow.propTypes = {
  ...NmdFlex.propTypes
}

NmdRow.defaultProps = {
  mx: '12px',
  flexWrap: 'wrap'
}
