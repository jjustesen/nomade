import styled from 'styled-components'

import { NmdBox } from '../Box'

export const NmdCol = styled(NmdBox)``

NmdCol.displayName = 'NmdCol'

NmdCol.propTypes = {
  ...NmdBox.propTypes
}

NmdCol.defaultProps = {
  px: '10px',
  my: '10px',
  width: 1
}
