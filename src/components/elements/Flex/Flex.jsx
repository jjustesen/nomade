import systemPropTypes from '@styled-system/prop-types'

import styled from 'styled-components'
import { flexbox, border } from 'styled-system'

import { NmdBox } from '../Box'

export const NmdFlex = styled(NmdBox)`
  ${flexbox};
  ${border};
`

NmdFlex.displayName = 'NmdFlex'

NmdFlex.propTypes = {
  ...NmdBox.propTypes,
  ...systemPropTypes.flexbox
}

NmdFlex.defaultProps = {
  display: 'flex'
}
