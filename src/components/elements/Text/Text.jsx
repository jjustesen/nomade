import systemPropTypes from '@styled-system/prop-types'
import PropTypes from 'prop-types'

import styled from 'styled-components'
import { typography, space, color, fontFamily, fontWeight, textAlign, letterSpacing, lineHeight } from 'styled-system'

export const NmdText = styled.span`
  ${space};
  ${color};
  ${fontFamily};
  ${fontWeight};
  ${textAlign};
  ${letterSpacing};
  ${lineHeight};
  ${typography};
`

NmdText.displayName = 'NmdText'

NmdText.propTypes = {
  truncate: PropTypes.bool,
  ...systemPropTypes.space,
  ...systemPropTypes.color,
  ...systemPropTypes.typography
}

NmdText.defaultProps = {
  variant: 'default',
  fontFamily: 'default'
}
