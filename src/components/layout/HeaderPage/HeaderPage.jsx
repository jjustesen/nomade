import React from 'react'
import styled from 'styled-components'
import { NmdBox } from '../../elements/Box'
import { NmdFlex } from '../../elements/Flex'
import { NmdText } from '../../elements/Text'

export const NmdHeaderPage = ({ nome = 'Johannes' }) => {
  const NmdAvatar = styled(NmdBox)`
    width: 48px;
    height: 48px;
    border-radius: 60px;
  `

  return (
    <NmdFlex justifyContent="space-between" alignItems="center" py={18} px={24}>
      <NmdBox backgroundColor="black" width={82} height={32} />

      <NmdFlex>
        <NmdFlex flexDirection="column" alignItems="flex-end">
          <NmdText fontSize="sm" fontFamily="montserrat" fontWeight="600">
            {`Olá ${nome}`}
          </NmdText>
          <NmdText fontSize="sx" fontFamily="montserrat" fontWeight="600" opacity="50%">
            Boa tarde!
          </NmdText>
        </NmdFlex>
        <NmdAvatar backgroundColor="primary" ml={16} />
      </NmdFlex>
    </NmdFlex>
  )
}

export default NmdHeaderPage
