import React from 'react'
import styled from 'styled-components'
import { NmdFlex } from '../../elements/Flex'
import { NmdText } from '../../elements/Text'

export const NmdButton = ({ label, color = 'black', ...props }) => {
  const NmdButton = styled(NmdFlex)`
    border-radius: 26px;
    overflow: hidden;
    height: fit-content;
    width: fit-content;

    cursor: pointer;
    :hover {
      background-color: #cdcdcd;
    }
  `

  return (
    <NmdButton {...props} backgroundColor="white" py={12} px={24}>
      <NmdText fontSize="sm" fontWeight="600" color={color}>
        {label}
      </NmdText>
    </NmdButton>
  )
}

export default NmdButton
