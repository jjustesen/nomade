import React, { memo, forwardRef } from 'react'

import PropTypes from 'prop-types'

import styled from 'styled-components'

import { useTheme } from '@sponte/lib-utils/dist/hooks/useTheme'

import { fontSize, ifProp } from '@sponte/lib-utils/dist/theme/tools'

import { SptBox } from '../../elements/Box'

export const NmdIconStyled = styled(SptBox)`
  ${fontSize()};
  flex-shrink: 0;
  stroke-width: 0;
  vertical-align: middle;
  backface-visibility: hidden;

  &:not(:root) {
    overflow: hidden;
  }

  cursor: ${ifProp('onClick', 'pointer', 'unset')};
`

NmdIconStyled.displayName = 'NmdIconStyled'

export const NmdIcon = memo(
  forwardRef((props, ref) => {
    const theme = useTheme()

    const icon = theme.icons[props.children] || {}

    return (
      <NmdIconStyled ref={ref} as="svg" display="inline-block" viewBox={icon.viewBox || '0 0 24 24'} {...props}>
        {icon.path}
      </NmdIconStyled>
    )
  })
)

NmdIcon.displayName = 'NmdIcon'

NmdIcon.propTypes = {
  color: PropTypes.oneOfType([PropTypes.number, PropTypes.string, PropTypes.array, PropTypes.object]),
  fontSize: PropTypes.oneOfType([PropTypes.number, PropTypes.string, PropTypes.array, PropTypes.object])
}

NmdIcon.defaultProps = {
  color: 'darkGrey',
  fontSize: 'xlarge',
  role: 'presentation',
  focusable: false,
  width: '1em',
  height: '1em'
}
